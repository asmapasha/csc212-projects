/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		/* TODO: write this. */
		this->root = 0;
		listNode** p = &root;
		*p = 0;
		for (listNode* pL = L.root; pL != 0 ; pL = pL->next) {
			*p = new listNode(pL->data);
			p = &(*p)->next;
		}
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		/* TODO: write this. */
		listNode** p = &root;
		*p = 0;
		for (listNode* pL = L.root; pL != 0 ; pL = pL->next) {
			*p = new listNode(pL->data);
			p = &(*p)->next;
		}
		return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{
	
		listNode* iterA = L1.root;
		listNode* iterB = L2.root;
		
		while (iterA && iterB){
		if(iterA->data != iterB->data)
				return false;
		
			iterA = iterA->next;
			iterB = iterB->next;
		
		}
		
		if (!iterA && !iterB)
		return true;	
		
		return true;	
	}
	
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)
	{
		// Creates a new node holding x and pointing to null
		listNode* newNode = new listNode(x);
		// Checks if the list is empty
		if (this->isEmpty()) {
			root = newNode;
			return;
		}

		listNode** current = &root;
		while((*current)!=0){
			if (x<=(*current)->data) {
				newNode->next = (*current);
				(*current) = newNode;
				return;
			}
			if ((*current)->next == 0) {
				(*current)->next = newNode;
				return;
			}
			current= &(*current)->next;
		}
	}

	void list::remove(val_type x)
	{
        if(!root) return;
        
        listNode *p1 = root;
        listNode *p2 = p1;
        
        while(p1) {
            if (root->data == x){
                root = root->next;
                return;
            }
            if (p1->data == x) {
                p2->next = p1->next;
                delete p1;
                return;
            }
            
            p2 = p1;
            p1 = p1->next;
        }
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p) 
			return true;
		else 
			return false;
	}

	unsigned long list::length() const
	{
		if(!root) return 0;
        
        listNode* p = root;
		unsigned long count = 0;
		while(p){
			count++;
			p = p->next;
		}
		return count;
	}

	void list::merge(const list& L1, const list& L2)
	{
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
/* 		This algorithm runs in linear time because there are no for loops or while loops within each of the three main while
 *  	loops. The sum of the iterations of each while loop must equal to the sum of the number of nodes of both L1 and L3 plus 3
 *  	(since the 3 main while loops must run at least once). Hence the run time is O(n). 
 */
		if (!this->isEmpty())
			this->clear();

		listNode* p1 = L1.root;				// points to the first list
		listNode* p2 = L2.root; 			// points to the second list
		listNode** p = &(this->root); 		// points to the pointer of the list that will merge the first and second list

		while(p1 && p2){
			listNode* newNode = new listNode();
			if(p1->data <= p2->data){
				newNode->data = p1->data;
				p1 = p1->next;
			}
			else{
				newNode->data = p2->data;
				p2 = p2->next;
			}
			*p = newNode;
			p = &(*p)->next;
		}
		while(p1){
			*p = new listNode(p1->data);
			p = &(*p)->next;
			p1 = p1->next;
		}
		while(p2){
			*p = new listNode(p2->data);
			p = &(*p)->next;
			p2 = p2->next;	
		}
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2)
	{
		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
		
/* 		Let L1={a1,a2,a3,a4,...,an} and
 * 			L2={b1,b2,b3,b4,...,bn}
 * 		
 * 		The worst case is when all the data in the listnodes of each list is unique such that the following occurs:
 * 		a1 < b1 < a2 < b2 < ... < an < bn
 * 		Then p1 and p2 point through every single listNode in its respective lists for a total of 2n times.
 * 		Thus the running time of this algorithm is O(n).
 */
		if (!this->isEmpty())
			this->clear();

		listNode* p1 = L1.root;				// points to the first list
		listNode* p2 = L2.root; 			// points to the second list
		listNode** p = &(this->root);		// points to the current list

		while (p1 && p2){
			if (p1->data == p2->data) {
				*p = new listNode(p1->data);
				p = &(*p)->next;

				p1 = p1->next;
				p2 = p2->next;
				continue;
			}
			if (p1->data > p2->data) 
				p2 = p2->next;
			else
				p1 = p1->next;
		}
	}
}
